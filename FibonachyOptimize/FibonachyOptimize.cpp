// FibonachyOptimize.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <time.h>
#include <cassert>
#include <iostream>
//#include <unordered_map>
#include <vector>

class Fibonacci
{
public:
    static int get(int n){
        assert(n>=0); // ���� �������, �� ������������ ������ � ������� ����� �� ���������
        if (n == 0 || n == 1) return n;
       //get(n - 2) + get(n-1);  
        // �� ��� ���� ��������� ��������, �� ������ �� �������� �� ������������ ���-�� ���������
        // � ������. � ������� ������� ���� �������� �������� ����� � �� ����� ������ �������. 
        // � ��� ����� ������ ��� ������ ����� ���������.
        // �� ����� ������� ������������ ������������ ������
        /*********************************************/
       // static std::unordered_map<int, int> cache;
        // auto &result = cache[n]; //� �++11 auto ��������� �� ��������� ��� ���������� ����, ������ �����������, ����� �� ��� ��������� ����������� ��� ����������,
        // if (result == 0) result = get(n - 2) + get(n - 1);
        // // �������� - ��� ���� �����, ������ ��� ������ ����� ����������� ����� �������� ��������� ������������ �� ����� ������
        // // ������ ����� ������� ������ ����� ���. ������� ��� ������� ������� ��������, ��������� ������
        // return result;
        /*********************************************/
        // ������� ������� �������� �� ���������������� ���������
        //cache[0] = 0;
        //cache[1] = 1;
        //for (int i = 2; i <= n; i++)
        //{
        //    cache[i] = cache[i - 2] + cache[i - 1];

        //}
        //return cache[n];
        /*********************************************/
        // �� ����� ������� ��� �������, �������� ���� ��������� � ������� �� vector
        //std::vector<int> cache;
        //cache.resize(n+1); // �������� ������ ������� �������
        //cache[0] = 0;
        //cache[1] = 1;
        //for (int i = 2; i <= n; i++)
        //{
        //    cache[i] = cache[i - 2] + cache[i - 1];

        //}
        //return cache[n]; 
        /*********************************************/
        // ������ ����� ��� ��������. ������� �� ��� ���������� ��������, � ������ ���� ��� ���������
        // ��� ���� � ������ ����
        // ������� �������� ����������� ������ ��-�� �������� ������ ���� �����
        int previous = 0;
        int current = 1;
        for (int i = 2; i <= n; i++)
        {
            int new_current = previous + current;
            previous = current;
            current = new_current;
        }
        return current;
    }


};


int main(int argc, char* argv[])
{
    clock_t tStart = clock();
    /* Do your stuff here */
    int n;
    std::cin >> n;
    std::cout << Fibonacci::get(n) << std::endl;
    printf("Time taken: %.2fs\n", (double)(clock() - tStart) / CLOCKS_PER_SEC);
    std::cin >> n;
	return 0;
}

