// BIMinexDll.h

//#ifdef BIMinexDLL_EXPORTS
//#define BIMinexLL_API __declspec(dllexport) 
//#else
//#define BIMinexDLL_API __declspec(dllimport) 
//#endif
#ifndef __BI_h__
#define __BI_h__


typedef unsigned char uchar; //!!! ��� ������ᨬ��� �� ��権 ���������

#define MAX_MINUT   128

static const int si[] = {
    -1902, -1891, -1879, -1867, -1854, -1841, -1827, -1813, -1798, -1782,
    -1766, -1749, -1732, -1714, -1696, -1677, -1658, -1638, -1618, -1597,
    -1576, -1554, -1532, -1509, -1486, -1463, -1439, -1414, -1389, -1364,
    -1338, -1312, -1286, -1259, -1231, -1204, -1176, -1147, -1118, -1089,
    -1060, -1030, -1000, -970, -939, -908, -877, -845, -813, -781,
    -749, -717, -684, -651, -618, -585, -551, -518, -484, -450,
    -416, -382, -347, -313, -278, -244, -209, -174, -140, -105,
    -70, -35, 0, 35, 70, 105, 140, 174, 209, 244,
    278, 313, 347, 382, 416, 450, 484, 518, 551, 585,
    618, 651, 684, 717, 749, 781, 813, 845, 877, 908,
    939, 970, 1000, 1030, 1060, 1089, 1118, 1147, 1176, 1204,
    1231, 1259, 1286, 1312, 1338, 1364, 1389, 1414, 1439, 1463,
    1486, 1509, 1532, 1554, 1576, 1597, 1618, 1638, 1658, 1677,
    1696, 1714, 1732, 1749, 1766, 1782, 1798, 1813, 1827, 1841,
    1854, 1867, 1879, 1891, 1902 };

static const int co[] = {
    618, 651, 684, 717, 749, 781, 813, 845, 877, 908,
    939, 970, 1000, 1030, 1060, 1089, 1118, 1147, 1176, 1204,
    1231, 1259, 1286, 1312, 1338, 1364, 1389, 1414, 1439, 1463,
    1486, 1509, 1532, 1554, 1576, 1597, 1618, 1638, 1658, 1677,
    1696, 1714, 1732, 1749, 1766, 1782, 1798, 1813, 1827, 1841,
    1854, 1867, 1879, 1891, 1902, 1913, 1923, 1932, 1941, 1949,
    1956, 1963, 1970, 1975, 1981, 1985, 1989, 1992, 1995, 1997,
    1999, 2000, 2000, 2000, 1999, 1997, 1995, 1992, 1989, 1985,
    1981, 1975, 1970, 1963, 1956, 1949, 1941, 1932, 1923, 1913,
    1902, 1891, 1879, 1867, 1854, 1841, 1827, 1813, 1798, 1782,
    1766, 1749, 1732, 1714, 1696, 1677, 1658, 1638, 1618, 1597,
    1576, 1554, 1532, 1509, 1486, 1463, 1439, 1414, 1389, 1364,
    1338, 1312, 1286, 1259, 1231, 1204, 1176, 1147, 1118, 1089,
    1060, 1030, 1000, 970, 939, 908, 877, 845, 813, 781,
    749, 717, 684, 651, 618 };

static int   CO;            // Compare POVOROT()
static int   SI;            // Compare POVOROT()
static int   dx;            // Compare POVOROT()
static int   dy;            // Compare POVOROT()
static int   fit;           // Compare POVOROT()
static int   n1;            // Compare POVOROT()
static int   x1[MAX_MINUT]; // Compare POVOROT()
static int   yy1[MAX_MINUT]; // Compare POVOROT() //!!! � MSVC ���� �㭪�� y1(...)
static int   a1[MAX_MINUT]; // Compare POVOROT()
static int   n2;            // Compare POVOROT()
static int   x2[MAX_MINUT]; // Compare POVOROT()
static int   y2[MAX_MINUT]; // Compare POVOROT()
static int   a2[MAX_MINUT]; // Compare POVOROT()
static int   a1t[MAX_MINUT];// Compare POVOROT
static int   x1t[MAX_MINUT];// Compare POVOROT
static int   y1t[MAX_MINUT];// Compare POVOROT
static int   q1[150];       // Compare FMR2FCT POVOROT
static int   q2[150];       // POVOROT
static int   w1[MAX_MINUT]; // Compare FMR2FCT POVOROT
static int   w2[MAX_MINUT]; // POVOROT
static uchar z0[41][35];    // POVOROT
static uchar z1[41][35];    // POVOROT
static uchar z2[41][35];    // POVOROT
static uchar z[23][23][23]; // Compare

int Compare(uchar *pap, uchar *pbp);
int function();
#endif